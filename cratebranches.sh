#!/bin/bash

set -e

[ "x${JENKINS_URL}" == "x" ] && JENKINS_URL=http://localhost:8080/
JENKINS_USER=admin
JENKINS_PASS=not2easy
JENKINS_URL="http://localhost:8080"
GIT="https://gitlab.com/mcyrrer/mavenrelease.git"
TEMPLATE="buildperbranch/Template"
TOJOBBASEPATH="buildperbranch/"

#XML=`which xmlstarlet` || { echo "please install xmlstarlet and try again"; exit 2; }

isJavaAvail(){ # find java
if [ "x${JAVA_HOMa}" != "x" ]; then JAVA_CMD="${JAVA_HOME}/bin/java/"
else
  for candidate in /etc/alternatives/java /usr/lib/jvm/java-1.6.0/bin/java /usr/lib/jvm/jre-1.6.0/bin/java /usr/lib/jvm/java-1.5.0/bin/java /usr/lib/jvm/jre-1.5.0/bin/java /usr/bin/java
    do
    [ -x "$JAVA_CMD" ] && break
     JAVA_CMD="$candidate"
    done
fi
[ -x "$JAVA_CMD" ] || { printf "[ $FUNCNAME ] no java found, exiting $0 \n"; exit 2; } && printf "[ $FUNCNAME ] Using java executable Found in: $JAVA_CMD \n"
}

execJenkinsCli(){ # excute jenkins-cli jar
        jenkins_clinet_jar=jenkins-cli.jar
        # download only if it's newwer than the local version [ curl -z ], the cli jar should be ofthe smae jenkins version ...
        #echo "Downloading jenkins-cli.jar"

 #       curl -s -z jenkins-cli.jar http://localhost:8080/jnlpJars/jenkins-cli.jar -o ${jenkins_clinet_jar} || { echo "Echo  cannot obtain jenkins-cli.jar exiting"; exit 2;}
        cli_cmd=$1
        cmd_params=$2
        cmd_prefix="${JAVA_CMD} -jar ${jenkins_clinet_jar} -s ${JENKINS_URL}"

        #echo $cmd_prefix
        ${cmd_prefix} who-am-i | grep 'Authenticated as: anonymous' &>/dev/null && anon=0
        #anon=$?
        if [ "$anon" != "0" ]; then
                echo "crating job 1...."

                #printf "[ $FUNCNAME ] using jenkins-cli with private key auth"
                #echo  "${cmd_prefix} ${cli_cmd} ${cmd_params}"
                exec ${cmd_prefix} ${cli_cmd} ${cmd_params}
                #echo "hi2"
        elif [ "x" = "x${JENKINS_USER}" ] || [ "x"  = "x${JENKINS_PASS}" ]; then
                echo "crating job 2...."

                printf "[ $FUNCNAME ] cannot determin jenkins credentials\n";
                exit 3;
        else
                echo "crating job 3...."
                exec ${cmd_prefix} ${cli_cmd} ${cmd_params} --username ${JENKINS_USER} --password ${JENKINS_PASS}
        fi
}

generateJob(){ # generates a jenkins job
        echo "New branch will be created ${BRANCH}"
      #  if [ $# -lt 2 ] ; then
      #    echo  ""
      #    exit 1
      #  fi
        new_job_name=${TOJOBBASEPATH}${BRANCH}
        tmpl_job_name=${TEMPLATE}
        scm_url=${GIT}
        echo "Get Job..."

        execJenkinsCli copy-job "${tmpl_job_name} ${new_job_name}"
        #execJenkinsCli get-job ${tmpl_job_name} | execJenkinsCli create-job ${new_job_name}
       # echo $exit > tmp.xml
        echo "Create Job..."
       # `execJenkinsCli create-job ${new_job_name} < ${exit}`
        ##execJenkinsCli get-job ${tmpl_job_name}  | ${XML} ed -O -S -P -u '//project/scm/locations/hudson.scm.SubversionSCM_-ModuleLocation/remote' -v ${scm_url} | execJenkinsCli create-job ${new_job_name}
        execJenkinsCli enable-job ${new_job_name}
}

findBranches(){
    echo "Branches in git: ${GIT}"
    branchesraw=`git ls-remote --heads --refs ${GIT}`
    while read -r line; do
        BRANCH=${line##*/}
        if [[ ${testmystring} != *"$BRANCH"* ]];then
                    generateJob $BRANCH
        el
           echo "Branch already exist...."
        fi
        #echo $BRANCH
    done <<< "$branchesraw"
}


curl -s -z ./jenkins-cli.jar ${JENKINS_URL}/jnlpJars/jenkins-cli.jar -o jenkins-cli.jar || { echo "Echo  cannot obtain jenkins-cli.jar exiting"; exit 2;}



isJavaAvail

#EXISTINGBRANCHES=execJenkinsCli list-jobs buildperbranch
EXISTINGBRANCHES=exec java -jar jenkins-cli.jar -s http://localhost:8080/ list-jobs buildperbranch


echo "Time to find branches"
#findBranches

#branches=findBranches ${3}
#echo "Time to generate jobs"
#generateJob "$@" $branches
#generateJob buildperbranch/job/B1 buildperbranch/job/Template https://mcyrrer@gitlab.com/mcyrrer/mavenrelease.git